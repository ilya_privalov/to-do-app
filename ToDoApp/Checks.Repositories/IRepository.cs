﻿using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Threading;
using System.Threading.Tasks;
using JetBrains.Annotations;

namespace Checks.Repositories
{
    public interface IRepository<TEntity> where TEntity : class
    {
        [UsedImplicitly]
        Task<IReadOnlyCollection<TEntity>> GetAllAsync();

        Task<IReadOnlyCollection<TEntity>> GetWhereAsync(Expression<Func<TEntity, bool>> filter);

        ValueTask<TEntity> GetByIdAsync(object id);

        Task<TEntity> GetSingleOrDefaultAsync(Expression<Func<TEntity, bool>> filter);

        void Create(TEntity entity);

        void Update(TEntity entity);

        void Delete(TEntity entity);
    }
}