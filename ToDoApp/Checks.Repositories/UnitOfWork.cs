﻿using System;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using JetBrains.Annotations;
using Microsoft.EntityFrameworkCore;

namespace Checks.Repositories
{
    [UsedImplicitly]
    public class UnitOfWork : IUnitOfWork
    {
        private readonly IDictionary<Type, object> _repositories;
        private bool _isDisposed;

        protected readonly DbContext Context;


        public UnitOfWork(DbContext context)
        {
            Context = context;

            _repositories = new Dictionary<Type, object>();
        }


        public IRepository<TEntity> GetRepository<TEntity>() where TEntity : class
        {
            if (_repositories.TryGetValue(typeof(TEntity), out var existingRepository))
            {
                return (IRepository<TEntity>)existingRepository;
            }

            var newRepository = CreateRepository<TEntity>();

            _repositories.Add(typeof(TEntity), newRepository);

            return newRepository;
        }

        public async Task SaveAsync()
        {
            await Context.SaveChangesAsync();
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }


        protected virtual IRepository<TEntity> CreateRepository<TEntity>() where TEntity : class
        {
            return new Repository<TEntity>(Context);
        }


        private void Dispose(bool isDisposing)
        {
            if (!_isDisposed && isDisposing)
            {
                Context.Dispose();
            }

            _isDisposed = true;
        }
    }
}