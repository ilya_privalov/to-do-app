﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading;
using System.Threading.Tasks;
using JetBrains.Annotations;
using Microsoft.EntityFrameworkCore;

namespace Checks.Repositories
{
    [UsedImplicitly]
    public class Repository<TEntity> : IRepository<TEntity> where TEntity : class
    {
        private readonly DbSet<TEntity> _dbSet;

        protected readonly DbContext Context;


        public Repository(DbContext context)
        {
            Context = context;

            _dbSet = context.Set<TEntity>();
        }


        public async Task<IReadOnlyCollection<TEntity>> GetAllAsync()
        {
            return await GetAllQuery().ToListAsync();
        }

        public async Task<IReadOnlyCollection<TEntity>> GetWhereAsync(Expression<Func<TEntity, bool>> filter)
        {
            return await GetAllQuery().Where(filter).ToListAsync();
        }

        public virtual ValueTask<TEntity> GetByIdAsync(object id)
        {
            return _dbSet.FindAsync(id);
        }

        public Task<TEntity> GetSingleOrDefaultAsync(Expression<Func<TEntity, bool>> filter)
        {
            return GetAllQuery().SingleOrDefaultAsync(filter);
        }

        public void Create(TEntity entity)
        {
            _dbSet.Add(entity);
        }

        public void Update(TEntity entity)
        {
            _dbSet.Attach(entity);
            Context.Entry(entity).State = EntityState.Modified;
        }

        public void Delete(TEntity entity)
        {
            _dbSet.Attach(entity);
            _dbSet.Remove(entity);
        }


        protected virtual IQueryable<TEntity> GetAllQuery()
        {
            return _dbSet;
        }

        protected IQueryable<TEntity> GetQuery(params Expression<Func<TEntity, object>>[] includes)
        {
            return includes
                .Aggregate<Expression<Func<TEntity, object>>, IQueryable<TEntity>>(
                    _dbSet,
                    (current, include) => current.Include(include));
        }
    }
}
