﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Threading;
using JetBrains.Annotations;

namespace Checks.Common
{
    public struct ListOfOftenOne<T> : IReadOnlyCollection<T> where T : class
    {
        private object? _value;


        public int Count
        {
            get
            {
                return _value switch
                {
                    null => 0,
                    T singleValue => 1,
                    _ => ((T[])_value).Length
                };
            }
        }


        IEnumerator IEnumerable.GetEnumerator()
        {
            return GetEnumerator();
        }

        public IEnumerator<T> GetEnumerator()
        {
            return new Enumerator(Volatile.Read(ref _value));
        }

        public void Add(T value)
        {
            object? priorValue;
            object? fieldBeforeExchange;
            do
            {
                priorValue = Volatile.Read(ref _value);
                var newValue = Combine(priorValue, value);
                fieldBeforeExchange = Interlocked.CompareExchange(ref _value, newValue, priorValue);
            }
            while (priorValue != fieldBeforeExchange);
        }

        private static object Combine(object? baseValue, [System.Diagnostics.CodeAnalysis.NotNull]T value)
        {
            if (baseValue is null)
            {
                return value;
            }

            if (baseValue is T singleValue)
            {
                return new[] { singleValue, value };
            }

            var oldArray = (T[])baseValue;
            var result = new T[oldArray.Length + 1];

            oldArray.CopyTo(result, 0);
            result[^1] = value;

            return result;
        }



        [UsedImplicitly]
        public struct Enumerator : IEnumerator<T>
        {
            private const int IndexBeforeFirstArrayElement = -1;
            private const int IndexSingleElement = -2;
            private const int IndexBeforeSingleElement = -3;

            private readonly object? _enumeratedValue;

            private  int _currentIndex;


            internal Enumerator(object? enumeratedValue)
            {
                _enumeratedValue = enumeratedValue;
                _currentIndex = 0;
                Reset();
            }


            object IEnumerator.Current => Current;

            public T Current
            {
                get
                {
                    if (_currentIndex == IndexBeforeFirstArrayElement || _currentIndex == IndexBeforeSingleElement)
                    {
                        throw new InvalidOperationException();
                    }

                    return _currentIndex == IndexSingleElement
                        ? (T)_enumeratedValue!
                        : ((T[])_enumeratedValue!)[_currentIndex];
                }
            }


            public bool MoveNext()
            {
                switch (_currentIndex)
                {
                    case IndexBeforeSingleElement when _enumeratedValue is { }:
                        _currentIndex = IndexSingleElement;
                        return true;
                    case IndexSingleElement:
                        return false;
                    case IndexBeforeFirstArrayElement:
                        _currentIndex = 0;
                        return true;
                }

                var array = (T[]?)_enumeratedValue;
                if (_currentIndex < 0 || _currentIndex >= array!.Length)
                {
                    return false;
                }
                _currentIndex++;

                return _currentIndex < array.Length;
            }

            public void Reset()
            {
                _currentIndex = _enumeratedValue is T[] ? IndexBeforeFirstArrayElement : IndexBeforeSingleElement;
            }

            public void Dispose()
            {

            }
        }
    }
}