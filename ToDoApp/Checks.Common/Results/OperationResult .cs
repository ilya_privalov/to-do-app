﻿using System;

namespace Checks.Common.Results
{
    public class OperationResult<TError>
    {
        protected readonly TError ErrorField;


        public bool IsSuccessful { get; }

        public TError Error
        {
            get
            {
                if (IsSuccessful)
                {
                    throw new InvalidOperationException("Error cannot be accessed when result is successful.");
                }

                return ErrorField;
            }
        }


        protected OperationResult(bool isSuccessful, TError error)
        {
            IsSuccessful = isSuccessful;
            ErrorField = error;
        }


        public static implicit operator OperationResult<TError>(TError error)
        {
            return CreateUnsuccessfulWith(error);
        }

        public static OperationResult<TError> CreateSuccessful()
        {
            return new OperationResult<TError>(true, default!);
        }


        private static OperationResult<TError> CreateUnsuccessfulWith(TError error)
        {
            return new OperationResult<TError>(false, error);
        }
    }
}