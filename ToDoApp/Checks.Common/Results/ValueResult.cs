﻿using System;

namespace Checks.Common.Results
{
    public class ValueResult<TValue, TError> : OperationResult<TError>
    {
        private readonly TValue _value;


        public TValue Value
        {
            get
            {
                if (!IsSuccessful)
                {
                    throw new InvalidOperationException("Value cannot be accessed when result is not successful.");
                }

                return _value;
            }
        }


        private ValueResult(bool isSuccessful, TValue value, TError error)
            : base(isSuccessful, error)
        {
            _value = value;
        }


        public new static ValueResult<TValue, TError> CreateSuccessful()
        {
            return CreateSuccessful(default!);
        }

        public static implicit operator ValueResult<TValue, TError>(TValue result)
        {
            return CreateSuccessful(result);
        }

        public static implicit operator ValueResult<TValue, TError>(TError error)
        {
            return CreateUnsuccessfulWith(error);
        }


        private static ValueResult<TValue, TError> CreateUnsuccessfulWith(TError error)
        {
            return new ValueResult<TValue, TError>(false, default!, error );
        }

        private static ValueResult<TValue, TError> CreateSuccessful(TValue result)
        {
            return new ValueResult<TValue, TError>(true, result, default!);
        }
    }
}