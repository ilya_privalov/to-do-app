﻿using Microsoft.Extensions.Logging;

namespace Checks.Common.Logging
{
    public class Logger : ILogger
    {
        private readonly Microsoft.Extensions.Logging.ILogger _logger;


        public Logger(Microsoft.Extensions.Logging.ILogger logger)
        {
            _logger = logger;
        }


        public void Log(string message, params object[] parameters)
        {
            _logger.LogInformation(message, parameters);
        }
    }
}