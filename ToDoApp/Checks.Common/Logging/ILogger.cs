﻿namespace Checks.Common.Logging
{
    public interface ILogger
    {
        void Log(string message, params object[] parameters);
    }
}