﻿using System;
using System.Collections.Generic;
using System.Linq.Expressions;

namespace Checks.Common
{
    public static class Reflector
    {
        public static string ExtractPropertyPath<TObj, TProp>(Expression<Func<TObj, TProp>> propertyExpression)
        {
            var memberNames = new List<string>();

            var memberExpression = propertyExpression.Body as MemberExpression;
            while (memberExpression != null)
            {
                memberNames.Add(memberExpression.Member.Name);
                memberExpression = memberExpression.Expression as MemberExpression;
            }
            memberNames.Reverse();

            return String.Join(".", memberNames.ToArray());
        }
    }
}