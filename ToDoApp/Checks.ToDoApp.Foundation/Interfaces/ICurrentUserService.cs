﻿namespace Checks.ToDoApp.Foundation.Interfaces
{
    public interface ICurrentUserService
    {
        public int UserId { get; }
    }
}