﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Checks.Common.Results;
using Checks.ToDoApp.DomainModel;
using Checks.ToDoApp.Foundation.ErrorHandling;

namespace Checks.ToDoApp.Foundation.Interfaces
{
    public interface IBoardService
    {
        Task<ValueResult<Board, ServiceError>> CreateForCurrentUserAsync(Board board);

        Task<IReadOnlyCollection<Board>> GetAllForCurrentUserAsync();

        Task<ValueResult<Board, ServiceError>> SetNameByIdAsync(int id, string name);

        Task<OperationResult<ServiceError>> DeleteByIdAsync(int id);
    }
}