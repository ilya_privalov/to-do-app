﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Checks.Common.Results;
using Checks.ToDoApp.DomainModel;
using Checks.ToDoApp.Foundation.ErrorHandling;

namespace Checks.ToDoApp.Foundation.Interfaces
{
    public interface ISwimlaneService
    {
        Task<ValueResult<Swimlane, ServiceError>> CreateOnBoardAsync(int boardId, Swimlane swimlane);

        Task<IReadOnlyCollection<Swimlane>> GetAllForBoard(int boardId);

        Task<ValueResult<Swimlane, ServiceError>> UpdateAsync(int boardId, Swimlane swimlane);

        Task<OperationResult<ServiceError>> DeleteByIdAsync(int id);
    }
}