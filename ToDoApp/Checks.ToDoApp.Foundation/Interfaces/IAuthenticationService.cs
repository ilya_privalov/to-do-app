﻿using System.Threading.Tasks;
using Checks.Common.Results;
using Checks.ToDoApp.DomainModel;
using Checks.ToDoApp.Foundation.ErrorHandling;
using Microsoft.IdentityModel.Tokens;

namespace Checks.ToDoApp.Foundation.Interfaces
{
    public interface IAuthenticationService
    {
        SymmetricSecurityKey SecretKey { get; }

        Task<OperationResult<ServiceError>> RegisterAsync(User user, string password);

        Task<ValueResult<string, ServiceError>> GetTokenAsync(string userName, string password);
    }
}