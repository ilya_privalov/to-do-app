﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Checks.Common.Results;
using Checks.Repositories;
using Checks.ToDoApp.DomainModel;
using Checks.ToDoApp.Foundation.ErrorHandling;
using Checks.ToDoApp.Foundation.Interfaces;
using Checks.ToDoApp.Repositories.Interfaces;
using JetBrains.Annotations;

namespace Checks.ToDoApp.Foundation.Implementations
{
    [UsedImplicitly]
    public sealed class SwimlaneService : ISwimlaneService
    {
        private readonly IToDoAppUnitOfWork _unitOfWork;
        private readonly ISwimlaneRepository _swimlaneRepository;
        private readonly IRepository<Board> _boardRepository;
        private readonly ICurrentUserService _currentUserService;


        public SwimlaneService(IToDoAppUnitOfWork unitOfWork, ICurrentUserService currentUserService)
        {
            _unitOfWork = unitOfWork;
            _swimlaneRepository = unitOfWork.SwimlaneRepository;
            _boardRepository = unitOfWork.GetRepository<Board>();
            _currentUserService = currentUserService;
        }


        public async Task<ValueResult<Swimlane, ServiceError>> CreateOnBoardAsync(int boardId, Swimlane swimlane)
        {
            var board = await _boardRepository.GetByIdAsync(boardId);
            if (board == null)
            {
                return ServiceError
                    .WithCode(ServiceError.Codes.EntityDoesNotExist)
                    .WithMessage("Board with this {0} does not exists.")
                    .WithTarget(nameof(board))
                    .Create();
            }

            if (board.UserId != _currentUserService.UserId)
            {
                return CreateAccessDeniedError();
            }

            var swimlanes = await _swimlaneRepository
                .GetWhereAsync(entity => entity.Name == swimlane.Name && entity.BoardId == boardId);
            if (swimlanes.Any())
            {
                return ServiceError
                    .WithCode(ServiceError.Codes.EntityAlreadyExists)
                    .WithMessage("Swimlane with the same {0} already exists on the board.")
                    .WithTarget<Swimlane, string>(swimlane => swimlane.Name)
                    .Create();
            }

            swimlane.BoardId = boardId;
            _swimlaneRepository.Create(swimlane);
            await _unitOfWork.SaveAsync();

            return swimlane;
        }

        public async Task<IReadOnlyCollection<Swimlane>> GetAllForBoard(int boardId)
        {
            var swimlanes = await _swimlaneRepository.GetWhereAsync(swimlane => swimlane.BoardId == boardId);

            return swimlanes;
        }

        public async Task<ValueResult<Swimlane, ServiceError>> UpdateAsync(int boardId, Swimlane swimlane)
        {
            var board = await _boardRepository.GetByIdAsync(boardId);
            if (board == null)
            {
                return ServiceError
                    .WithCode(ServiceError.Codes.EntityDoesNotExist)
                    .WithMessage("Board with this {0} does not exists.")
                    .WithTarget(nameof(board))
                    .Create();
            }

            if (board.UserId != _currentUserService.UserId)
            {
                return CreateAccessDeniedError();
            }

            var swimlanesOnBoard = await _swimlaneRepository.GetWhereAsync(s => s.BoardId == boardId);
            if (swimlanesOnBoard.Any(s => s.Name == swimlane.Name))
            {
                return ServiceError.WithCode(ServiceError.Codes.EntityAlreadyExists)
                    .WithMessage("Swimlane with same name already exists on board.")
                    .Create();
            }

            if (swimlanesOnBoard.Any(s => s.Order == swimlane.Order))
            {
                return ServiceError.WithCode(ServiceError.Codes.EntityAlreadyExists)
                    .WithMessage("Swimlane with same order already exists on board.")
                    .Create();
            }

            _swimlaneRepository.Update(swimlane);
            await _unitOfWork.SaveAsync();

            return swimlane;
        }

        public async Task<OperationResult<ServiceError>> DeleteByIdAsync(int id)
        {
            var entity = await _swimlaneRepository.GetByIdAsync(id);
            if (entity == null)
            {
                return ServiceError
                    .WithCode(ServiceError.Codes.EntityDoesNotExist)
                    .WithMessage("The swimlane with this {0} does not exist.")
                    .WithTarget(nameof(id))
                    .Create();
            }

            var owningBoard = entity.Board;
            if (owningBoard.UserId != _currentUserService.UserId)
            {
                return CreateAccessDeniedError();
            }

            _swimlaneRepository.Delete(entity);
            await _unitOfWork.SaveAsync();

            return OperationResult<ServiceError>.CreateSuccessful();
        }


        private static ServiceError CreateAccessDeniedError()
        {
            return ServiceError
                .WithCode(ServiceError.Codes.AccessDenied)
                .WithMessage("Current user does not have access to this swimlane's board.")
                .Create();
        }
    }
}