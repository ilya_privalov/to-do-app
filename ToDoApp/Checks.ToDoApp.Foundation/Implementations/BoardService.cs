﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Checks.Common.Results;
using Checks.Repositories;
using Checks.ToDoApp.DomainModel;
using Checks.ToDoApp.Foundation.ErrorHandling;
using Checks.ToDoApp.Foundation.Interfaces;
using Checks.ToDoApp.Repositories.Interfaces;

namespace Checks.ToDoApp.Foundation.Implementations
{
    public class BoardService : IBoardService
    {
        private readonly IToDoAppUnitOfWork _unitOfWork;
        private readonly IRepository<Board> _repository;
        private readonly ICurrentUserService _currentUserService;


        public BoardService(IToDoAppUnitOfWork unitOfWork, ICurrentUserService currentUserService)
        {
            _unitOfWork = unitOfWork;
            _currentUserService = currentUserService;
            _repository = unitOfWork.GetRepository<Board>();
        }


        public async Task<ValueResult<Board, ServiceError>> CreateForCurrentUserAsync(Board board)
        {
            var boards = await _repository
                .GetWhereAsync(entity => entity.Name == board.Name);
            if (boards.Any())
            {
                return ServiceError
                    .WithCode(ServiceError.Codes.EntityAlreadyExists)
                    .WithMessage("Board with the same {0} already exists.")
                    .WithTarget<Board, string>(board => board.Name)
                    .Create();
            }

            board.UserId = _currentUserService.UserId;
            _repository.Create(board);
            await _unitOfWork.SaveAsync();

            return board;
        }

        public async Task<IReadOnlyCollection<Board>> GetAllForCurrentUserAsync()
        {
            var userId = _currentUserService.UserId;
            var boards = await _repository.GetWhereAsync(board => board.UserId == userId);

            return boards;
        }

        public async Task<ValueResult<Board, ServiceError>> SetNameByIdAsync(int id, string name)
        {
            var entity = await _repository.GetByIdAsync(id);
            if (entity == null)
            {
                return CreateNotExistsWithId(nameof(id));
            }

            if (entity.UserId != _currentUserService.UserId)
            {
                return CreateAccessDeniedWithId(nameof(id));
            }

            entity.Name = name;
            _repository.Update(entity);
            await _unitOfWork.SaveAsync();

            return entity;
        }

        public async Task<OperationResult<ServiceError>> DeleteByIdAsync(int id)
        {
            var entity = await _repository.GetByIdAsync(id);
            if (entity == null)
            {
                return CreateNotExistsWithId(nameof(id));
            }

            if (entity.UserId != _currentUserService.UserId)
            {
                return CreateAccessDeniedWithId(nameof(id));
            }

            _repository.Delete(entity);
            await _unitOfWork.SaveAsync();

            return OperationResult<ServiceError>.CreateSuccessful();
        }


        private static ServiceError CreateNotExistsWithId(string targetName)
        {
            return ServiceError
                .WithCode(ServiceError.Codes.EntityDoesNotExist)
                .WithMessage("Board with this {0} does not exist.")
                .WithTarget(targetName)
                .Create();
        }

        private static ServiceError CreateAccessDeniedWithId(string targetName)
        {
            return ServiceError
                .WithCode(ServiceError.Codes.AccessDenied)
                .WithMessage("Current user does not have access to the board with this {0}")
                .WithTarget(targetName)
                .Create();
        }
    }
}