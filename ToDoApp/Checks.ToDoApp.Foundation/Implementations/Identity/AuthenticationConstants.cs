﻿namespace Checks.ToDoApp.Foundation.Implementations.Identity
{
    public static class AuthenticationConstants
    {
        public const string OwnBoardPolicy = "OwnBoardPolicy";

        public const string Issuer = "http://localhost:54060";
        public const string Audience = "http://localhost:54060";
    }
}