﻿using System;
using System.Threading;
using System.Threading.Tasks;
using Checks.Repositories;
using Checks.ToDoApp.DomainModel;
using Checks.ToDoApp.Repositories.Interfaces;
using JetBrains.Annotations;
using Microsoft.AspNetCore.Identity;

namespace Checks.ToDoApp.Foundation.Implementations.Identity
{
    [UsedImplicitly]
    public class UserStore : IUserPasswordStore<User>
    {
        private readonly IToDoAppUnitOfWork _unitOfWork;
        private readonly IRepository<User> _repository;


        public UserStore(IToDoAppUnitOfWork unitOfWork)
        {
            _unitOfWork = unitOfWork;
            _repository = unitOfWork.GetRepository<User>();
        }


        public async Task<IdentityResult> CreateAsync(User user, CancellationToken cancellationToken)
        {
            _repository.Create(user);
            await _unitOfWork.SaveAsync();

            return IdentityResult.Success;
        }

        public Task<string> GetUserIdAsync(User user, CancellationToken cancellationToken)
        {
            var id = user.Id.ToString();

            return Task.FromResult(id);
        }

        public Task<string> GetUserNameAsync(User user, CancellationToken cancellationToken)
        {
            return Task.FromResult(user.UserName);
        }

        public Task SetUserNameAsync(User user, string userName, CancellationToken cancellationToken)
        {
            user.UserName = userName;

            return Task.CompletedTask;
        }

        public Task<string> GetNormalizedUserNameAsync(User user, CancellationToken cancellationToken)
        {
            return Task.FromResult(user.NormalizedUserName);
        }

        public Task SetNormalizedUserNameAsync(User user, string normalizedName, CancellationToken cancellationToken)
        {
            user.NormalizedUserName = normalizedName;

            return Task.CompletedTask;
        }

        public async Task<IdentityResult> UpdateAsync(User user, CancellationToken cancellationToken)
        {
            _repository.Update(user);
            await _unitOfWork.SaveAsync();

            return IdentityResult.Success;
        }

        public async Task<IdentityResult> DeleteAsync(User user, CancellationToken cancellationToken)
        {
            _repository.Delete(user);
            await _unitOfWork.SaveAsync();

            return IdentityResult.Success;
        }

        public Task<User> FindByIdAsync(string userId, CancellationToken cancellationToken)
        {
            var id = Int32.Parse(userId);

            return _repository.GetByIdAsync(id).AsTask();
        }

        public Task<User> FindByNameAsync(string normalizedUserName, CancellationToken cancellationToken)
        {
            return _repository.GetSingleOrDefaultAsync(user => user.NormalizedUserName == normalizedUserName);
        }

        public Task SetPasswordHashAsync(User user, string passwordHash, CancellationToken cancellationToken)
        {
            user.PasswordHash = passwordHash;

            return Task.CompletedTask;
        }

        public Task<string> GetPasswordHashAsync(User user, CancellationToken cancellationToken)
        {
            return Task.FromResult(user.PasswordHash);
        }

        public Task<bool> HasPasswordAsync(User user, CancellationToken cancellationToken)
        {
            var hasPasswordHash = user.PasswordHash != null;

            return Task.FromResult(hasPasswordHash);
        }

        public void Dispose()
        {

        }
    }
}