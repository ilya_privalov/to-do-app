﻿using System;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Checks.Common.Results;
using Checks.ToDoApp.DomainModel;
using Checks.ToDoApp.Foundation.ErrorHandling;
using Checks.ToDoApp.Foundation.Interfaces;
using JetBrains.Annotations;
using Microsoft.AspNetCore.Identity;
using Microsoft.Extensions.Configuration;
using Microsoft.IdentityModel.Tokens;

namespace Checks.ToDoApp.Foundation.Implementations.Identity
{
    [UsedImplicitly]
    public class AuthenticationService : IAuthenticationService
    {
        private const int TokenExpirationHours = 1;
        private const string ApplicationSecretEnvironmentVariableKey = "TODOAPP:JWT_SECRET";
        private const string TokenEncryptionAlgorithm = SecurityAlgorithms.HmacSha256;

        private readonly UserManager<User> _userManager;
        private readonly IUserClaimsPrincipalFactory<User> _claimsPrincipalFactory;
        private readonly IConfiguration _configuration;


        public SymmetricSecurityKey SecretKey
        {
            get
            {
                var secret = _configuration[ApplicationSecretEnvironmentVariableKey];
                if (secret == null)
                {
                    throw new NullReferenceException($"The {ApplicationSecretEnvironmentVariableKey} environment variable must be set.");
                }

                var secretBytes = Encoding.UTF8.GetBytes(secret);
                var key = new SymmetricSecurityKey(secretBytes);

                return key;
            }
        }


        public AuthenticationService(
            UserManager<User> userManager,
            IConfiguration configuration,
            IUserClaimsPrincipalFactory<User> claimsPrincipalFactory)
        {
            _userManager = userManager;
            _configuration = configuration;
            _claimsPrincipalFactory = claimsPrincipalFactory;
        }


        public async Task<OperationResult<ServiceError>> RegisterAsync(User user, string password)
        {
            var result = await _userManager.CreateAsync(user, password);

            return result.Succeeded ? OperationResult<ServiceError>.CreateSuccessful() : GetErrorFrom(result);
        }

        public async Task<ValueResult<string, ServiceError>> GetTokenAsync(string userName, string password)
        {
            var user = await _userManager.FindByNameAsync(userName);
            if (user == null)
            {
                return ServiceError
                    .WithCode(ServiceError.Codes.UserNotFound)
                    .WithMessage("User with this {0} does not exist.")
                    .WithTarget(nameof(userName))
                    .Create();
            }

            var isPasswordCorrect = await _userManager.CheckPasswordAsync(user, password);
            if (!isPasswordCorrect)
            {
                return ServiceError
                    .WithCode(ServiceError.Codes.IncorrectPassword)
                    .WithMessage("The {0} is invalid.")
                    .WithTarget(nameof(password))
                    .Create();
            }

            var claimsPrincipal = await _claimsPrincipalFactory.CreateAsync(user);
            var signingCredentials = new SigningCredentials(SecretKey, TokenEncryptionAlgorithm);
            var token = new JwtSecurityToken(
                AuthenticationConstants.Issuer,
                AuthenticationConstants.Audience,
                claimsPrincipal.Claims,
                expires: DateTime.Now.AddHours(TokenExpirationHours),
                signingCredentials: signingCredentials);
            var tokenString = new JwtSecurityTokenHandler().WriteToken(token);

            return tokenString;
        }


        private static ServiceError GetErrorFrom(IdentityResult identityResult)
        {
            var identityDetails = identityResult
                .Errors
                .Select(ie => ServiceError.WithCode(ie.Code).WithMessage(ie.Description).Create())
                .ToList();

            var error = ServiceError
                .WithCode(ServiceError.Codes.SignUpError)
                .WithMessage("Error in sign up credentials.")
                .WithDetails(identityDetails)
                .Create();

            return error;
        }
    }
}