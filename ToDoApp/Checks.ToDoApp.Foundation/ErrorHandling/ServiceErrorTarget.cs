﻿namespace Checks.ToDoApp.Foundation.ErrorHandling
{
    public sealed class ServiceErrorTarget
    {
        public string Argument { get; }

        public string Path { get; }


        public ServiceErrorTarget(string argument, string path)
        {
            Argument = argument;
            Path = path;
        }
    }
}