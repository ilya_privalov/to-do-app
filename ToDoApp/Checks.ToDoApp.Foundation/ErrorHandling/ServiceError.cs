﻿using System.Collections.Generic;
using Checks.ToDoApp.Foundation.ErrorHandling.Builders;

namespace Checks.ToDoApp.Foundation.ErrorHandling
{
    public sealed class ServiceError
    {
        public string Code { get; set; }

        public string Message { get; set; }

        public ServiceErrorTarget Target { get; set; }

        public IReadOnlyCollection<ServiceError> Details { get; set; }


        public static ErrorWithMessageBuilder WithCode(string errorCode)
        {
            var errorResult = new ServiceError {Code = errorCode};

            return new ErrorWithMessageBuilder(errorResult);
        }



        public static class Codes
        {
            public const string EntityAlreadyExists = "EntityAlreadyExists";
            public const string EntityDoesNotExist = "EntityDoesNotExist";
            public const string SignUpError = "SignUpError";
            public const string UserNotFound = "UserNotFound";
            public const string IncorrectPassword = "IncorrectPassword";
            public const string AccessDenied = "AccessDenied";
        }
    }
}