﻿using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using Checks.Common;
using Checks.Common.Results;

namespace Checks.ToDoApp.Foundation.ErrorHandling.Builders
{
    public sealed class ErrorWithTargetAndOrDetailsBuilder : ErrorResultBaseBuilder
    {
        private const string MemberSeparator = ".";


        public ErrorWithTargetAndOrDetailsBuilder(ServiceError serviceError)
            : base(serviceError)
        {

        }


        public ErrorWithTargetAndOrDetailsBuilder WithTarget(string targetName)
        {
            ServiceError.Target = new ServiceErrorTarget(targetName, targetName);

            FormatMessageWithTarget();

            return this;
        }

        public ErrorWithTargetAndOrDetailsBuilder WithTarget<TObj, TProp>(Expression<Func<TObj, TProp>> propertyExpression)
        {
            var path = Reflector.ExtractPropertyPath(propertyExpression);
            var argument = path.Split(MemberSeparator)[0];
            ServiceError.Target = new ServiceErrorTarget(argument, path);

            FormatMessageWithTarget();

            return this;
        }

        public ErrorWithTargetAndOrDetailsBuilder WithDetails(IReadOnlyCollection<ServiceError> errors)
        {
            ServiceError.Details = errors;

            return this;
        }

        public ServiceError Create()
        {
            return ServiceError;
        }


        private void FormatMessageWithTarget()
        {
            var message = ServiceError.Message;
            if (!String.IsNullOrEmpty(message))
            {
                ServiceError.Message = String.Format(message, ServiceError.Target.Path);
            }
        }
    }
}