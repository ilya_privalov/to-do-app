﻿namespace Checks.ToDoApp.Foundation.ErrorHandling.Builders
{
    public sealed class ErrorWithMessageBuilder : ErrorResultBaseBuilder
    {
        public ErrorWithMessageBuilder(ServiceError serviceError)
            : base(serviceError)
        {

        }


        public ErrorWithTargetAndOrDetailsBuilder WithMessage(string message)
        {
            ServiceError.Message = message;

            return new ErrorWithTargetAndOrDetailsBuilder(ServiceError);
        }
    }
}