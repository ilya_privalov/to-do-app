﻿namespace Checks.ToDoApp.Foundation.ErrorHandling.Builders
{
    public class ErrorResultBaseBuilder
    {
        protected ServiceError ServiceError { get; }


        protected ErrorResultBaseBuilder(ServiceError serviceError)
        {
            ServiceError = serviceError;
        }
    }
}