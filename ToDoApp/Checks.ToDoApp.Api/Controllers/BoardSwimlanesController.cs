﻿using System.Threading.Tasks;
using Checks.ToDoApp.DomainModel;
using Checks.ToDoApp.Foundation.Implementations.Identity;
using Checks.ToDoApp.Foundation.Interfaces;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using ToDoApp.Api.DataContracts.Errors;
using ToDoApp.Api.DataContracts.Responses;
using ToDoApp.Api.Mapping;

namespace ToDoApp.Api.Controllers
{
    [ApiController]
    [Authorize]
    [Route("api/v{version:apiVersion}/boards/{boardId:int}/swimlanes", Name = "Route")]
    public class BoardSwimlanesController : ControllerBase
    {
        private readonly ISwimlaneService _swimlaneService;
        private readonly ICommonMapper _mapper;


        public BoardSwimlanesController(ISwimlaneService swimlaneService, ICommonMapper mapper)
        {
            _swimlaneService = swimlaneService;
            _mapper = mapper;
        }

        [HttpPost]
        [Authorize(Policy = AuthenticationConstants.OwnBoardPolicy)]
        [ProducesResponseType(typeof(SwimlaneDataContract), StatusCodes.Status201Created)]
        [ProducesResponseType(typeof(ErrorBody), StatusCodes.Status400BadRequest)]
        public async Task<IActionResult> Post(int boardId, SwimlaneDataContract swimlane)
        {
            var model = _mapper.MapTo<Swimlane>(swimlane);
            var result = await _swimlaneService.CreateOnBoardAsync(boardId, model);
            if (!result.IsSuccessful)
            {
                return _mapper.MapError(result.Error);
            }

            var contract = _mapper.MapTo<SwimlaneDataContract>(result.Value);

            return Created(Url.Link("Route", new { boardId }), contract);
        }

        [HttpGet]
        [ProducesResponseType(typeof(SwimlaneDataContract), StatusCodes.Status200OK)]
        public async Task<IActionResult> Get(int boardId)
        {
            var result = await _swimlaneService.GetAllForBoard(boardId);

            return Ok(_mapper.MapMany<SwimlaneDataContract>(result));
        }
    }
}