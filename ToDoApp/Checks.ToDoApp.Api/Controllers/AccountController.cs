﻿using System.ComponentModel.DataAnnotations;
using System.Threading.Tasks;
using Checks.ToDoApp.DomainModel;
using Checks.ToDoApp.Foundation.Interfaces;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using ToDoApp.Api.DataContracts.Errors;
using ToDoApp.Api.DataContracts.Requests;
using ToDoApp.Api.DataContracts.Responses;
using ToDoApp.Api.Mapping;

namespace ToDoApp.Api.Controllers
{
    [ApiController]
    [Route("api/v{version:apiVersion}/[controller]")]
    public class AccountController : ControllerBase
    {
        private readonly IAuthenticationService _authenticationService;
        private readonly ICommonMapper _mapper;


        public AccountController(IAuthenticationService authenticationService, ICommonMapper mapper)
        {
            _authenticationService = authenticationService;
            _mapper = mapper;
        }

        [HttpPost]
        [ProducesResponseType(typeof(ErrorBody), StatusCodes.Status400BadRequest)]
        public async Task<IActionResult> Post(UserPostDataContract user)
        {
            var userModel = new User
            {
                UserName = user.UserName,
                NormalizedUserName = user.UserName.ToUpperInvariant()
            };
            var result = await _authenticationService.RegisterAsync(userModel, user.Password);

            return result.IsSuccessful ? Ok() : _mapper.MapError(result.Error);
        }

        [Route("~/api/token")]
        [HttpGet]
        [ProducesResponseType(typeof(ErrorBody), StatusCodes.Status400BadRequest)]
        [ProducesResponseType(typeof(TokenDataContract), StatusCodes.Status200OK)]
        public async Task<IActionResult> Get(
            [Required] string userName,
            [Required] string password)
        {
            var result = await _authenticationService.GetTokenAsync(userName, password);
            var token = new TokenDataContract { AccessToken = result.Value };

            return !result.IsSuccessful ? _mapper.MapError(result.Error) : Ok(token);
        }
    }
}