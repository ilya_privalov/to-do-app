﻿using System.ComponentModel.DataAnnotations;
using System.Threading.Tasks;
using Checks.ToDoApp.DomainModel;
using Checks.ToDoApp.Foundation.Implementations.Identity;
using Checks.ToDoApp.Foundation.Interfaces;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Cors;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using ToDoApp.Api.DataContracts.Errors;
using ToDoApp.Api.DataContracts.Requests;
using ToDoApp.Api.DataContracts.Responses;
using ToDoApp.Api.Mapping;

namespace ToDoApp.Api.Controllers
{
    [ApiController]
    [Route("api/v{version:apiVersion}/[controller]")]
    public class BoardsController : ControllerBase
    {
        private readonly IBoardService _boardService;
        private readonly ICommonMapper _mapper;


        public BoardsController(IBoardService boardService, ICommonMapper mapper)
        {
            _boardService = boardService;
            _mapper = mapper;
        }


        [HttpPost]
        [ProducesResponseType(typeof(BoardDataContract), StatusCodes.Status201Created)]
        [ProducesResponseType(typeof(ErrorBody), StatusCodes.Status400BadRequest)]
        public async Task<IActionResult> Post(BoardPostDataContract board)
        {
            var model = _mapper.MapTo<Board>(board);
            var result = await _boardService.CreateForCurrentUserAsync(model);
            if (!result.IsSuccessful)
            {
                return _mapper.MapError(result.Error);
            }
            var contract = _mapper.MapTo<BoardDataContract>(result.Value);

            return CreatedAtAction(nameof(Get), new {id = contract.Id}, contract);
        }

        [HttpGet]
        [ProducesResponseType(typeof(BoardDataContract), StatusCodes.Status200OK)]
        public async Task<IActionResult> Get()
        {
            var models = await _boardService.GetAllForCurrentUserAsync();
            var contracts = _mapper.MapMany<BoardDataContract>(models);

            return Ok(contracts);
        }

        [HttpPatch("{id:int}")]
        [Authorize]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(typeof(ErrorBody), StatusCodes.Status404NotFound)]
        public async Task<IActionResult> Patch(
            [Required]
            [Range(0, int.MaxValue, ErrorMessage = Error.Messages.IdFieldShouldNotBeZero)]
            int id,
            BoardPatchDataContract board)
        {
            var result = await _boardService.SetNameByIdAsync(board.Id, board.Name);

            return !result.IsSuccessful ? _mapper.MapError(result.Error) : Ok();
        }

        [HttpDelete("{id:int}")]
        [Authorize]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(typeof(ErrorBody), StatusCodes.Status404NotFound)]
        public async Task<IActionResult> Delete(
            [Required]
            [Range(0, int.MaxValue, ErrorMessage = Error.Messages.IdFieldShouldNotBeZero)]
            int id)
        {
            var result = await _boardService.DeleteByIdAsync(id);

            return !result.IsSuccessful ? _mapper.MapError(result.Error) : Ok();
        }
    }
}