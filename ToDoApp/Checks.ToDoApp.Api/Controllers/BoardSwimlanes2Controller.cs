﻿using System.Threading.Tasks;
using Checks.ToDoApp.DomainModel;
using Checks.ToDoApp.Foundation.Interfaces;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using ToDoApp.Api.DataContracts.Errors;
using ToDoApp.Api.DataContracts.Responses;
using ToDoApp.Api.Mapping;

namespace ToDoApp.Api.Controllers
{
    [ApiController]
    [Authorize]
    [ApiVersion("2.0")]
    [Route("api/v{version:apiVersion}/boards/{boardId:int}/swimlanes", Name = "Route")]
    public class BoardSwimlanes2Controller : ControllerBase
    {
        private readonly ISwimlaneService _swimlaneService;
        private readonly ICommonMapper _mapper;


        public BoardSwimlanes2Controller(ISwimlaneService swimlaneService, ICommonMapper mapper)
        {
            _swimlaneService = swimlaneService;
            _mapper = mapper;
        }

        [HttpPost]
        [ProducesResponseType(typeof(Swimlane2DataContract), StatusCodes.Status201Created)]
        [ProducesResponseType(typeof(ErrorBody), StatusCodes.Status400BadRequest)]
        public async Task<IActionResult> Post(int boardId, Swimlane2DataContract swimlane)
        {
            var model = _mapper.MapTo<Swimlane>(swimlane);
            var result = await _swimlaneService.CreateOnBoardAsync(boardId, model);
            if (!result.IsSuccessful)
            {
                return _mapper.MapError(result.Error);
            }

            var contract = _mapper.MapTo<Swimlane2DataContract>(result.Value);

            return Created(Url.Link("Route", new { boardId }), contract);
        }

        [HttpGet]
        [ProducesResponseType(typeof(Swimlane2DataContract), StatusCodes.Status200OK)]
        public async Task<IActionResult> Get(int boardId)
        {
            var result = await _swimlaneService.GetAllForBoard(boardId);

            return Ok(_mapper.MapMany<Swimlane2DataContract>(result));
        }

        [HttpPut]
        [ProducesResponseType(typeof(Swimlane2DataContract), StatusCodes.Status201Created)]
        [ProducesResponseType(typeof(ErrorBody), StatusCodes.Status400BadRequest)]
        public async Task<IActionResult> Put(int boardId, Swimlane2DataContract swimlane)
        {
            var model = _mapper.MapTo<Swimlane>(swimlane);
            var result = await _swimlaneService.UpdateAsync(boardId, model);
            if (!result.IsSuccessful)
            {
                return _mapper.MapError(result.Error);
            }

            var contract = _mapper.MapTo<Swimlane2DataContract>(result.Value);

            return Created(Url.Link("Route", new { boardId }), contract);
        }
    }
}