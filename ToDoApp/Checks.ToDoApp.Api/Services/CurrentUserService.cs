﻿using System;
using System.Security.Claims;
using Checks.ToDoApp.Foundation.Interfaces;
using Microsoft.AspNetCore.Http;

namespace ToDoApp.Api.Services
{
    public sealed class CurrentUserService : ICurrentUserService
    {
        private readonly IHttpContextAccessor _httpContextAccessor;


        public CurrentUserService(IHttpContextAccessor httpContextAccessor)
        {
            _httpContextAccessor = httpContextAccessor;
        }


        public int UserId
        {
            get
            {
                var id = _httpContextAccessor.HttpContext?.User.FindFirstValue(ClaimTypes.NameIdentifier);

                return Int32.TryParse(id, out var result) ? result : 0;
            }
        }
    }
}