﻿using System;
using System.Diagnostics;
using System.Threading.Tasks;
using Checks.Repositories;
using Checks.ToDoApp.DomainModel;
using Checks.ToDoApp.Foundation.Interfaces;
using Checks.ToDoApp.Repositories.Interfaces;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;

namespace ToDoApp.Api.Services
{
    public class UserNameEqualsRequirementHandler : AuthorizationHandler<UserCanOnlyAccessOwnBoardRequirement>
    {
        private readonly IRepository<Board> _repository;
        private readonly ICurrentUserService _currentUserService;


        public UserNameEqualsRequirementHandler(IToDoAppUnitOfWork unitOfWork, ICurrentUserService currentUserService)
        {
            _currentUserService = currentUserService;
            _repository = unitOfWork.GetRepository<Board>();
        }


        protected async override Task HandleRequirementAsync(
            AuthorizationHandlerContext context,
            UserCanOnlyAccessOwnBoardRequirement requirement)
        {
            if (context.Resource is not DefaultHttpContext httpContext)
            {
                return;
            }

            var request = httpContext.Request;
            var routeValues = request.RouteValues;
            var boardId = routeValues["boardId"];
            var board = await _repository.GetByIdAsync(Int32.Parse(boardId.ToString()));
            if (board.UserId == _currentUserService.UserId)
            {
                context.Succeed(requirement);
            }
        }
    }
}