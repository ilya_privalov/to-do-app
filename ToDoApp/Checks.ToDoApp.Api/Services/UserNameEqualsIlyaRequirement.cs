﻿using Microsoft.AspNetCore.Authorization;

namespace ToDoApp.Api.Services
{
    public sealed class UserCanOnlyAccessOwnBoardRequirement : IAuthorizationRequirement
    {

    }
}