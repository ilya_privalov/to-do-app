﻿using System.Threading;
using System.Threading.Tasks;
using Microsoft.Extensions.Diagnostics.HealthChecks;

namespace ToDoApp.Api.HealthChecks
{
    public sealed class ExampleHealthCheck : IHealthCheck
    {
        public async Task<HealthCheckResult> CheckHealthAsync(
            HealthCheckContext context,
            CancellationToken cancellationToken = default)
        {
            await Task.Delay(100, cancellationToken);

            // return HealthCheckResult.Healthy("The app is healthy!");
            return HealthCheckResult.Unhealthy("oops");
        }
    }
}