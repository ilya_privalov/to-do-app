﻿using System.Collections.Generic;
using System.Text.Json.Serialization;

namespace ToDoApp.Api.DataContracts.Errors
{
    public sealed class Error
    {
        [JsonIgnore(Condition = JsonIgnoreCondition.WhenWritingNull)]
        public string Code { get; set; }

        [JsonIgnore(Condition = JsonIgnoreCondition.WhenWritingNull)]
        public string Message { get; set; }

        [JsonIgnore(Condition = JsonIgnoreCondition.WhenWritingNull)]
        public string Target { get; set; }

        [JsonIgnore(Condition = JsonIgnoreCondition.WhenWritingNull)]
        public IReadOnlyCollection<Error> Details { get; set; }



        public static class Codes
        {
            public const string InvalidArgument = "InvalidArgument";
        }

        public static class Messages
        {
            public const string StringTooLong = "The {0} is longer than {1}.";
            public const string StringTooShort = "The {0} is shorter than {1}.";
            public const string IdFieldShouldNotBeZero = "The value of {0} should be greater than {1}.";
            public const string InvalidArgumentWithFormat = "The argument {0} is invalid.";
            public const string InvalidArgument = "An error in the argument.";
        }
    }
}