﻿using JetBrains.Annotations;

namespace ToDoApp.Api.DataContracts.Errors
{
    public sealed class ErrorBody
    {
        [UsedImplicitly]
        public Error Error { get; }


        private ErrorBody(Error error)
        {
            Error = error;
        }


        public static ErrorBody CreateFrom(Error error)
        {
            return new ErrorBody(error);
        }
    }
}