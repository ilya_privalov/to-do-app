﻿using System.ComponentModel.DataAnnotations;
using Checks.ToDoApp.DomainModel;
using Microsoft.AspNetCore.Mvc;
using ToDoApp.Api.DataContracts.Errors;

namespace ToDoApp.Api.DataContracts.Requests
{
    public sealed class BoardPatchDataContract
    {
        [Required]
        [Range(0, int.MaxValue, ErrorMessage = Error.Messages.IdFieldShouldNotBeZero)]
        public int Id { get; set; }

        [Required]
        [MaxLength(Defaults.MaximumNameLength, ErrorMessage = Error.Messages.StringTooLong)]
        public string Name { get; set; }
    }
}