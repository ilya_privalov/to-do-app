﻿using System.ComponentModel.DataAnnotations;

namespace ToDoApp.Api.DataContracts.Requests
{
    public sealed class UserPostDataContract
    {
        [Required]
        public string UserName { get; set; }

        [Required]
        public string Password { get; set; }
    }
}