﻿using System.ComponentModel.DataAnnotations;
using Checks.ToDoApp.DomainModel;
using ToDoApp.Api.DataContracts.Errors;

namespace ToDoApp.Api.DataContracts.Requests
{
    public sealed class BoardPostDataContract
    {
        [Required]
        [MaxLength(Defaults.MaximumNameLength, ErrorMessage = Error.Messages.StringTooLong)]
        public string Name { get; set; }
    }
}