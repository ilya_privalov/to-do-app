﻿using System.ComponentModel.DataAnnotations;
using Checks.ToDoApp.DomainModel;
using ToDoApp.Api.DataContracts.Errors;

namespace ToDoApp.Api.DataContracts.Responses
{
    public class SwimlaneDataContract
    {
        [Required]
        [MaxLength(Defaults.MaximumNameLength, ErrorMessage = Error.Messages.StringTooLong)]
        public string Name { get; set; }

        [Required]
        public int Order { get; set; }
    }
}