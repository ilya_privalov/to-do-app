﻿using System.ComponentModel.DataAnnotations;
using Checks.ToDoApp.DomainModel;
using ToDoApp.Api.DataContracts.Errors;

namespace ToDoApp.Api.DataContracts.Responses
{
    public class Swimlane2DataContract
    {
        [Required]
        [MaxLength(Defaults.MaximumNameLength, ErrorMessage = Error.Messages.StringTooLong)]
        public string Name { get; set; }

        [Required]
        public int OrderOnBoard { get; set; }
    }
}