﻿namespace ToDoApp.Api.DataContracts.Responses
{
    public sealed class BoardDataContract
    {
        public int Id { get; set; }

        public string Name { get; set; }

        public int UserId { get; set; }
    }
}