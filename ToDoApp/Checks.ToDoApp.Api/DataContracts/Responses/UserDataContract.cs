﻿namespace ToDoApp.Api.DataContracts.Responses
{
    public sealed class UserDataContract
    {
        public int Id { get; set; }

        public string UserName { get; set; }
    }
}