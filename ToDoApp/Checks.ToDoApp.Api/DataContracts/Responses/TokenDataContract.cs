﻿using System.Text.Json.Serialization;

namespace ToDoApp.Api.DataContracts.Responses
{
    public sealed class TokenDataContract
    {
        [JsonPropertyName("access_token")]
        public string AccessToken { get; set; }
    }
}