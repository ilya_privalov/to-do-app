﻿using System.Collections.Generic;
using Checks.ToDoApp.Foundation.ErrorHandling;
using Microsoft.AspNetCore.Mvc;

namespace ToDoApp.Api.Mapping
{
    public interface ICommonMapper
    {
        TDestination MapTo<TDestination>(object sources);

        IReadOnlyCollection<TDestination> MapMany<TDestination>(IReadOnlyCollection<object> sourceCollection);

        IActionResult MapError(ServiceError error);
    }
}