﻿using AutoMapper;
using Checks.ToDoApp.DomainModel;
using JetBrains.Annotations;
using ToDoApp.Api.DataContracts.Responses;

namespace ToDoApp.Api.Mapping.Profiles
{
    [UsedImplicitly]
    public sealed class SwimlaneMappingProfile : Profile
    {
        public SwimlaneMappingProfile()
        {
            CreateMap<SwimlaneDataContract, Swimlane>();
            CreateMap<Swimlane, SwimlaneDataContract>();

            CreateMap<Swimlane2DataContract, Swimlane>()
                .ForMember(
                    swimlane => swimlane.Order,
                    configuration => configuration.MapFrom(contract => contract.OrderOnBoard));
            CreateMap<Swimlane, Swimlane2DataContract>()
                .ForMember(
                    contract => contract.OrderOnBoard,
                    configuration => configuration.MapFrom(swimlane => swimlane.Order));
        }
    }
}