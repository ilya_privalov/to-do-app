﻿using AutoMapper;
using Checks.ToDoApp.DomainModel;
using JetBrains.Annotations;
using ToDoApp.Api.DataContracts.Requests;
using ToDoApp.Api.DataContracts.Responses;

namespace ToDoApp.Api.Mapping.Profiles
{
    [UsedImplicitly]
    public sealed class BoardMappingProfile : Profile
    {
        public BoardMappingProfile()
        {
            CreateMap<BoardPostDataContract, Board>();
            CreateMap<Board, BoardDataContract>();
        }
    }
}