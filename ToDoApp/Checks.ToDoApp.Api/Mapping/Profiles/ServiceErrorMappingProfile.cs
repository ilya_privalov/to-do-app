﻿using AutoMapper;
using Checks.ToDoApp.Foundation.ErrorHandling;
using JetBrains.Annotations;
using ToDoApp.Api.DataContracts.Errors;

namespace ToDoApp.Api.Mapping.Profiles
{
    [UsedImplicitly]
    public sealed class ServiceErrorMappingProfile : Profile
    {
        public ServiceErrorMappingProfile()
        {
            CreateMap<ServiceError, Error>();
        }
    }
}