﻿using System;
using System.Collections.Generic;
using System.Linq;
using AutoMapper;
using Checks.ToDoApp.Foundation.ErrorHandling;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using ToDoApp.Api.DataContracts.Errors;

namespace ToDoApp.Api.Mapping
{
    public class CommonMapper : ICommonMapper
    {
        private static readonly IDictionary<string, int> ErrorCodeToStatusCodeMap;

        private readonly IMapper _mapper;


        static CommonMapper()
        {
            ErrorCodeToStatusCodeMap = new Dictionary<string, int>
            {
                { ServiceError.Codes.EntityAlreadyExists, StatusCodes.Status400BadRequest },
                { ServiceError.Codes.EntityDoesNotExist, StatusCodes.Status404NotFound },
                { ServiceError.Codes.SignUpError, StatusCodes.Status400BadRequest }
            };
        }

        public CommonMapper(IMapper mapper)
        {
            _mapper = mapper;
        }


        public TDestination MapTo<TDestination>(object source)
        {
            return _mapper.Map<TDestination>(source);
        }

        public IReadOnlyCollection<TDestination> MapMany<TDestination>(IReadOnlyCollection<object> sources)
        {
            var destinations =  sources
                .Select(MapTo<TDestination>)
                .ToList();

            return destinations;
        }

        public IActionResult MapError(ServiceError serviceError)
        {
            var statusCode = ErrorCodeToStatusCodeMap[serviceError.Code];
            var apiResult = new ObjectResult(null) { StatusCode = statusCode };

            var error = new Error
            {
                Code = Error.Codes.InvalidArgument,
                Details = GetDetailsFrom(serviceError)
            };

            if (serviceError.Target == null)
            {
                error.Message = Error.Messages.InvalidArgument;
                apiResult.Value = ErrorBody.CreateFrom(error);

                return apiResult;
            }

            var argument = serviceError.Target.Argument;
            error.Message = String.Format(Error.Messages.InvalidArgumentWithFormat, argument);
            apiResult.Value = ErrorBody.CreateFrom(error);

            return apiResult;
        }


        private IReadOnlyCollection<Error> GetDetailsFrom(ServiceError serviceError)
        {
            var doesErrorHaveDetails = serviceError.Details?.Any() ?? false;

            return doesErrorHaveDetails
                ? new List<Error> { _mapper.Map<Error>(serviceError) }
                : null;
        }
    }
}