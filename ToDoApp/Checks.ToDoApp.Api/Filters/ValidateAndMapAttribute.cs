﻿using System.Collections.Generic;
using System.Linq;
using JetBrains.Annotations;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Filters;
using Microsoft.AspNetCore.Mvc.ModelBinding;
using ToDoApp.Api.DataContracts.Errors;

namespace ToDoApp.Api.Filters
{
    [UsedImplicitly]
    public class ValidateAndMapAttribute : ActionFilterAttribute
    {
        public override void OnActionExecuting(ActionExecutingContext context)
        {
            var modelState = context.ModelState;
            if (modelState.IsValid)
            {
                return;
            }
            var responseBody = GetErrorResponseFrom(modelState);

            context.Result = new BadRequestObjectResult(responseBody);
        }


        private static ErrorBody GetErrorResponseFrom(ModelStateDictionary modelState)
        {
            var details = modelState
                .Where(CheckIfInvalid)
                .SelectMany(GetErrorDetails).ToList();
            var error = new Error
            {
                Code = Error.Codes.InvalidArgument,
                Message = Error.Messages.InvalidArgument,
                Details = details
            };

            return ErrorBody.CreateFrom(error);
        }

        private static IEnumerable<Error> GetErrorDetails(KeyValuePair<string, ModelStateEntry> modelStateKeyValue)
        {
            var (target, value) = modelStateKeyValue;
            var validationErrors = value.Errors;
            var errors = validationErrors.Select(validationError => new Error
            {
                Message = validationError.ErrorMessage,
                Target = target
            });

            return errors.ToList();
        }

        private static bool CheckIfInvalid(KeyValuePair<string, ModelStateEntry> errorKeyValuePair)
            => errorKeyValuePair.Value.ValidationState == ModelValidationState.Invalid;
    }
}