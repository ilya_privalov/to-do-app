﻿using System.Diagnostics;
using System.Threading.Tasks;
using Checks.Common.Logging;
using Microsoft.AspNetCore.Http;

namespace ToDoApp.Api.Middleware
{
    public sealed class StopwatchMiddleware
    {
        private readonly RequestDelegate _next;
        private readonly ILogger _logger;

        private readonly Stopwatch _stopwatch;


        public StopwatchMiddleware(RequestDelegate next, ILogger logger)
        {
            _next = next;
            _logger = logger;
            _stopwatch = new Stopwatch();
        }


        public async Task InvokeAsync(HttpContext httpContext)
        {
            _stopwatch.Restart();
            _logger.Log("Request came");

            httpContext.Response.OnStarting(state => {
                _stopwatch.Stop();
                var elapsedMilliseconds = _stopwatch.ElapsedMilliseconds;
                _logger.Log($"Request took {elapsedMilliseconds} milliseconds to serve");

                var context = (HttpContext)state;
                context.Response.Headers.Add("X-serve-time", new[] { elapsedMilliseconds.ToString() });

                return Task.CompletedTask;
            }, httpContext);

            await _next(httpContext);
        }
    }
}