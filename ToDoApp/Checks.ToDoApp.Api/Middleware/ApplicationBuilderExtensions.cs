﻿using Microsoft.AspNetCore.Builder;

namespace ToDoApp.Api.Middleware
{
    public static class ApplicationBuilderExtensions
    {
        public static IApplicationBuilder UseStopwatch(this IApplicationBuilder applicationBuilder)
        {
            return applicationBuilder.UseMiddleware<StopwatchMiddleware>();
        }
    }
}