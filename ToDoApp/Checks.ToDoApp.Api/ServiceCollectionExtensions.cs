﻿using AutoMapper;
using Checks.Common.Logging;
using Checks.ToDoApp.DomainModel;
using Checks.ToDoApp.Foundation.Implementations;
using Checks.ToDoApp.Foundation.Implementations.Identity;
using Checks.ToDoApp.Foundation.Interfaces;
using Checks.ToDoApp.Repositories;
using Checks.ToDoApp.Repositories.Implementations;
using Checks.ToDoApp.Repositories.Interfaces;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.ApiExplorer;
using Microsoft.AspNetCore.Mvc.Versioning;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using Microsoft.IdentityModel.Tokens;
using Microsoft.OpenApi.Models;
using Serilog;
using ToDoApp.Api.Filters;
using ToDoApp.Api.HealthChecks;
using ToDoApp.Api.Mapping;
using ToDoApp.Api.Services;
using ILogger = Serilog.ILogger;

namespace ToDoApp.Api
{
    public static class ServiceCollectionExtensions
    {
        public static void AddDatabase(this IServiceCollection services, IConfiguration configuration)
        {
            var connectionString = configuration.GetConnectionString(ToDoAppDbContext.DataBaseName);
            services.AddDbContext<DbContext, ToDoAppDbContext>(options => options.UseSqlServer(connectionString));
            services.AddScoped<IToDoAppUnitOfWork, ToDoAppUnitOfWork>();
        }

        public static void AddApplicationHealthChecks(this IServiceCollection services)
        {
            services.AddHealthChecks()
                .AddDbContextCheck<DbContext>("DbContext")
                .AddCheck<ExampleHealthCheck>("Example");
        }

        public static void AddMapping(this IServiceCollection services)
        {
            var autoMapperConfig = new MapperConfiguration(mapperConfiguration =>
            {
                var mappersAssembly = typeof(CommonMapper).Assembly;
                mapperConfiguration.AddMaps(mappersAssembly);
            });
            var autoMapper = autoMapperConfig.CreateMapper();
            services.AddSingleton(autoMapper);
            services.AddSingleton<ICommonMapper, CommonMapper>();
        }

        public static void AddApplicationIdentity(this IServiceCollection services)
        {
            services.AddTransient<IAuthenticationService, AuthenticationService>();
            services.AddHttpContextAccessor();
            services.AddSingleton<ICurrentUserService, CurrentUserService>();
            services.Configure<IdentityOptions>(ConfigureIdentity);
            services.AddIdentityCore<User>().AddUserStore<UserStore>();
            services.AddTransient<IUserClaimsPrincipalFactory<User>, UserClaimsPrincipalFactory<User>>();

            var authenticationService = services.BuildServiceProvider().GetService<IAuthenticationService>();
            services
                .AddAuthentication(JwtBearerDefaults.AuthenticationScheme)
                .AddJwtBearer(options => ConfigureJwt(options, authenticationService));
            services.AddAuthorization(options =>
            {
                options.AddPolicy(
                    AuthenticationConstants.OwnBoardPolicy,
                    policy => policy.Requirements.Add(new UserCanOnlyAccessOwnBoardRequirement()));
            });
            services.AddTransient<IAuthorizationHandler, UserNameEqualsRequirementHandler>();
        }

        public static void AddDomainServices(this IServiceCollection services)
        {
            services.AddTransient<IBoardService, BoardService>();
            services.AddTransient<ISwimlaneService, SwimlaneService>();
        }

        public static void AddControllersAndFilters(this IServiceCollection services)
        {
            services.AddControllers(options =>
            {
                options.Filters.Add<ValidateAndMapAttribute>();
            });
            services.Configure<ApiBehaviorOptions>(opts => opts.SuppressModelStateInvalidFilter = true);
            services.AddApiVersioning(options =>
            {
                options.AssumeDefaultVersionWhenUnspecified = true;
                options.DefaultApiVersion = ApiVersion.Default;
                options.ReportApiVersions = true;
                options.ApiVersionReader = new UrlSegmentApiVersionReader();
            });
            services.AddVersionedApiExplorer(options =>
            {
                options.GroupNameFormat = "'v'VVV";
                options.SubstituteApiVersionInUrl = true;
            });
        }

        public static void AddLoggingAndConfigure(this IServiceCollection services, IConfiguration configuration)
        {
            services.AddLogging(builder =>
            {
                var serilogLogger = GetSerilogLogger(configuration);
                builder.AddSerilog(serilogLogger);
            });
            services.AddSingleton<Checks.Common.Logging.ILogger, Logger>();
            services.AddSingleton<Microsoft.Extensions.Logging.ILogger>(serviceProvider => serviceProvider.GetService<ILogger<Logger>>());
        }

        public static void AddSwagger(this IServiceCollection services)
        {
            services.AddSwaggerGen(options =>
            {
                options.AddSecurityDefinition("Bearer", new OpenApiSecurityScheme()
                {
                    In = ParameterLocation.Header,
                    Description = "Insert JWT with Bearer preceded by Bearer keyword",
                    Name = "Authorization",
                    Type = SecuritySchemeType.ApiKey
                });

                var securityScheme = new OpenApiSecurityScheme()
                {
                    Reference = new OpenApiReference
                    {
                        Type = ReferenceType.SecurityScheme,
                        Id = "Bearer"
                    }
                };
                options.AddSecurityRequirement(new OpenApiSecurityRequirement
                {
                    { securityScheme, System.Array.Empty<string>() }
                });

                var provider = services.BuildServiceProvider();
                var descriptionProvider = provider.GetRequiredService<IApiVersionDescriptionProvider>();
                foreach (var description in descriptionProvider.ApiVersionDescriptions)
                {
                    options.SwaggerDoc(description.GroupName, new OpenApiInfo
                    {
                        Title = "ToDoApp",
                        Version = description.ApiVersion.ToString(),
                        Description = "To do app api with versions"
                    });
                }
            });
        }


        private static ILogger GetSerilogLogger(IConfiguration configuration)
        {
            var logger = new LoggerConfiguration()
                .ReadFrom.Configuration(configuration)
                .CreateLogger();

            return logger;
        }

        private static void ConfigureIdentity(IdentityOptions options)
        {
            var password = options.Password;
            password.RequiredLength = 4;
            password.RequireUppercase = false;
            password.RequireLowercase = false;
            password.RequireDigit = false;
            password.RequireNonAlphanumeric = false;
            password.RequiredUniqueChars = 0;
        }

        private static void ConfigureJwt(JwtBearerOptions bearerOptions, IAuthenticationService authenticationService)
        {
            bearerOptions.TokenValidationParameters = new TokenValidationParameters()
            {
                ValidAudience = AuthenticationConstants.Audience,
                ValidIssuer = AuthenticationConstants.Issuer,
                IssuerSigningKey = authenticationService.SecretKey
            };
        }
    }
}