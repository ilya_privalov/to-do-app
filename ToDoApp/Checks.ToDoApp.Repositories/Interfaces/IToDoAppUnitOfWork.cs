﻿using Checks.Repositories;

namespace Checks.ToDoApp.Repositories.Interfaces
{
    public interface IToDoAppUnitOfWork : IUnitOfWork
    {
        ISwimlaneRepository SwimlaneRepository { get; }
    }
}