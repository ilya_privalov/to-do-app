﻿using Checks.Repositories;
using Checks.ToDoApp.DomainModel;

namespace Checks.ToDoApp.Repositories.Interfaces
{
    public interface ISwimlaneRepository : IRepository<Swimlane>
    {

    }
}