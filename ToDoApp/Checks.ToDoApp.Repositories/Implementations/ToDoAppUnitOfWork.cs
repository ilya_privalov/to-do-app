﻿using System;
using System.Collections.Generic;
using Checks.Repositories;
using Checks.ToDoApp.DomainModel;
using Checks.ToDoApp.Repositories.Interfaces;
using Microsoft.EntityFrameworkCore;

namespace Checks.ToDoApp.Repositories.Implementations
{
    public class ToDoAppUnitOfWork : UnitOfWork, IToDoAppUnitOfWork
    {
        private readonly IDictionary<Type, Type> _entityTypeToRepositoryTypeMap;


        public ISwimlaneRepository SwimlaneRepository => (SwimlaneRepository)CreateRepository<Swimlane>();


        public ToDoAppUnitOfWork(DbContext context)
            : base(context)
        {
            _entityTypeToRepositoryTypeMap = new Dictionary<Type, Type>
            {
                { typeof(Swimlane), typeof(SwimlaneRepository) },
            };
        }


        protected override IRepository<TEntity> CreateRepository<TEntity>()
        {
            var repository = _entityTypeToRepositoryTypeMap.TryGetValue(typeof(TEntity), out var repositoryType)
                ? (Repository<TEntity>)Activator.CreateInstance(repositoryType, Context)
                : base.CreateRepository<TEntity>();

            return repository;
        }
    }
}