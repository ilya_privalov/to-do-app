﻿using System.Linq;
using Checks.Repositories;
using Checks.ToDoApp.DomainModel;
using Checks.ToDoApp.Repositories.Interfaces;
using Microsoft.EntityFrameworkCore;

namespace Checks.ToDoApp.Repositories.Implementations
{
    public sealed class SwimlaneRepository : Repository<Swimlane>, ISwimlaneRepository
    {
        public SwimlaneRepository(DbContext context)
            : base(context)
        {

        }


        protected override IQueryable<Swimlane> GetAllQuery()
        {
            return GetQuery(swimlane => swimlane.Board);
        }
    }
}