﻿using Checks.ToDoApp.DomainModel;
using JetBrains.Annotations;
using Microsoft.EntityFrameworkCore;

namespace Checks.ToDoApp.Repositories
{
    public sealed class ToDoAppDbContext : DbContext
    {
        public const string DataBaseName = "ToDoAppDb";


        [UsedImplicitly]
        public DbSet<User> Users { get; set; }

        [UsedImplicitly]
        public DbSet<Board> Boards { get; set; }

        [UsedImplicitly]
        public DbSet<Swimlane> Swimlanes { get; set; }

        [UsedImplicitly]
        public DbSet<ToDoItem> ToDoItems { get; set; }


        public ToDoAppDbContext(DbContextOptions<ToDoAppDbContext> options)
            : base(options)
        {
            Database.EnsureCreated();
        }


        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            var configurationsAssembly = typeof(ToDoAppDbContext).Assembly;
            modelBuilder.ApplyConfigurationsFromAssembly(configurationsAssembly);
        }
    }
}