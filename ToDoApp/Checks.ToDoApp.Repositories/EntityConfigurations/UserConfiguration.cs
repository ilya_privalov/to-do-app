﻿using Checks.ToDoApp.DomainModel;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace Checks.ToDoApp.Repositories.EntityConfigurations
{
    public class UserConfiguration : IEntityTypeConfiguration<User>
    {
        public void Configure(EntityTypeBuilder<User> builder)
        {
            builder.Property(user => user.UserName).IsRequired().HasMaxLength(Defaults.MaximumNameLength);
            builder
                .HasMany(user => user.Boards)
                .WithOne(board => board.User)
                .OnDelete(DeleteBehavior.Cascade);
        }
    }
}