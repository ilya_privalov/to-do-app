﻿using Checks.ToDoApp.DomainModel;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace Checks.ToDoApp.Repositories.EntityConfigurations
{
    public class BoardConfiguration : IEntityTypeConfiguration<Board>
    {
        public void Configure(EntityTypeBuilder<Board> builder)
        {
            builder
                .Property(board => board.Name)
                .IsRequired()
                .HasMaxLength(Defaults.MaximumNameLength);
            builder
                .HasMany(board => board.Swimlanes)
                .WithOne(swimlane => swimlane.Board)
                .OnDelete(DeleteBehavior.Cascade);
        }
    }
}