﻿using Checks.ToDoApp.DomainModel;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace Checks.ToDoApp.Repositories.EntityConfigurations
{
    public class SwimlaneConfiguration : IEntityTypeConfiguration<Swimlane>
    {
        public void Configure(EntityTypeBuilder<Swimlane> builder)
        {
            builder.Property(swimlane => swimlane.Name).IsRequired().HasMaxLength(Defaults.MaximumNameLength);
            builder
                .HasMany(swimlane => swimlane.ToDoItems)
                .WithOne(item => item.Swimlane)
                .OnDelete(DeleteBehavior.Cascade);
        }
    }
}