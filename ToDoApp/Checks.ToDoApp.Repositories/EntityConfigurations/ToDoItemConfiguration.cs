﻿using Checks.ToDoApp.DomainModel;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace Checks.ToDoApp.Repositories.EntityConfigurations
{
    public class ToDoItemConfiguration : IEntityTypeConfiguration<ToDoItem>
    {
        public void Configure(EntityTypeBuilder<ToDoItem> builder)
        {
            builder.Property(item => item.Summary).IsRequired().HasMaxLength(ToDoItem.SummaryMaxLength);
        }
    }
}