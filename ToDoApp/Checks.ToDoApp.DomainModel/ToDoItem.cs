﻿using JetBrains.Annotations;

namespace Checks.ToDoApp.DomainModel
{
    [UsedImplicitly]
    public class ToDoItem
    {
        public const int SummaryMaxLength = 255;


        public int Id { get; [UsedImplicitly]set; }

        public string Summary { get; [UsedImplicitly]set; }

        public int SwimLaneId { get; [UsedImplicitly]set; }

        public Swimlane Swimlane { get;[UsedImplicitly] set; }
    }
}