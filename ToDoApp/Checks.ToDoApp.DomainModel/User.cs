﻿using System.Collections.Generic;
using JetBrains.Annotations;

namespace Checks.ToDoApp.DomainModel
{
    [UsedImplicitly]
    public class User
    {
        public int Id { get; [UsedImplicitly]set; }

        public string UserName { get; [UsedImplicitly]set; }

        public string NormalizedUserName { get; [UsedImplicitly]set; }

        public string PasswordHash { get; [UsedImplicitly]set; }

        public ICollection<Board> Boards { get; [UsedImplicitly]set; }
    }
}