﻿using System.Collections.Generic;
using JetBrains.Annotations;

namespace Checks.ToDoApp.DomainModel
{
    [UsedImplicitly]
    public class Board
    {
        public int Id { get; [UsedImplicitly]set; }

        public string Name { get; [UsedImplicitly]set; }

        public int UserId { get; [UsedImplicitly]set; }

        public User User { get; [UsedImplicitly]set; }

        public ICollection<Swimlane> Swimlanes { get; [UsedImplicitly]set; }
    }
}