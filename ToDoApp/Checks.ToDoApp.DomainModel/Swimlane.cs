﻿using System.Collections.Generic;
using JetBrains.Annotations;

namespace Checks.ToDoApp.DomainModel
{
    [UsedImplicitly]
    public class Swimlane
    {
        public int Id { get; [UsedImplicitly]set; }

        public string Name { get; [UsedImplicitly]set; }

        public int Order { get; [UsedImplicitly]set; }

        public int BoardId { get; [UsedImplicitly]set; }

        public Board Board { get; [UsedImplicitly]set; }

        public ICollection<ToDoItem> ToDoItems { get; [UsedImplicitly]set; }
    }
}